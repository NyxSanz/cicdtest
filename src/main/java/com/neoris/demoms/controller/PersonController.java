package com.neoris.demoms.controller;

import com.neoris.demoms.application.PersonApplication;
import com.neoris.demoms.entity.Persona;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("persona")
public class PersonController {

    @Autowired
    private PersonApplication personaApplication;


    @Value("${greeter.message}")
    private String greeterMessageFormat;

    @GetMapping("nonmbre")
    public String name(@RequestParam("name")String name){

        return "nombre es : " + name;

    }
    @GetMapping("edad")
    public String edad(@RequestParam("edad") String edad){

        return personaApplication.edad(edad);

    }

    @PostMapping()
    public Persona addPerson(@RequestBody Persona person){


        return this.personaApplication.addPerson(person);

    }

    @GetMapping("id")
    public Optional<Persona> getPerson(@RequestParam("id") String id){

        return this.personaApplication.getPerson(id);

    }
    @GetMapping("all")
    public List<Persona> getAll(){
        return this.personaApplication.findAll();
    }

    @GetMapping("config/{user}")
    public String config(@PathVariable("user")String user){

        String prefix = System.getenv().getOrDefault("VAR_ENVIROME", "WORK");

        if(prefix == null){
            prefix = "error";
        }
        return String.format(greeterMessageFormat,prefix, user);

    }

}
