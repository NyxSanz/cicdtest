package com.neoris.demoms.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collection = "Persona")
@Data
public class Persona implements Serializable {

    @Id
    private String id;

    private String nombre;

    private  Integer edad;

    private String direccion;

}


