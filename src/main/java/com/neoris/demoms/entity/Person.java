package com.neoris.demoms.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collation = "Persona")
public class Person implements Serializable {

    @Id
    private String id;

    private String nombre;

    private String edad;

}
