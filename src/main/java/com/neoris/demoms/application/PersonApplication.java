package com.neoris.demoms.application;

import com.neoris.demoms.entity.Person;
import com.neoris.demoms.entity.Persona;
import com.neoris.demoms.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class PersonApplication {


    @Autowired
    private PersonRepository personRepository;

    public String nombre(String nombre){

        return "name :" + nombre;

    }


    public String edad(String edad){

        return "edad :" + edad;

    }

    public Persona addPerson(Persona person){

        if(person == null){
            //todo exeption here, and check each element ecluide id

        }

        Persona personaDB = this.personRepository.save(person);

        return personaDB;

    }

    public Optional<Persona> getPerson(String id){

        if(null == id || "".equals(id)){
            //todo exeption here

        }

        Optional<Persona> personDb = this.personRepository.findById(id);

        return personDb;


    }

    public List<Persona> findAll(){

        List<Persona>  personList = (List<Persona>) this.personRepository.findAll();


        return personList;
    }


}
