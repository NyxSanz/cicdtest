package com.neoris.demoms.repository;

import com.neoris.demoms.entity.Persona;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends CrudRepository<Persona,String> {

    Iterable<Persona> findAll();



}
