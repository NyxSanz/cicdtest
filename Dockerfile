FROM openjdk11:ppc64le-ubi-minimal-jre11u-nightly
EXPOSE 8090
VOLUME /tmp
COPY target/*.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]